package ru.itcube64;

import java.util.ArrayList;

public class Storage {
    private final ArrayList<String> quoteList;

    public Storage() {
        this.quoteList = new ArrayList<>();

        this.quoteList.add("Начинать всегда стоит с того, что сеет сомнения.\nБорис Стругацкий");
        this.quoteList.add("80% успеха - это появиться в нужном месте в нужное время.\nВуди Аллен");
        this.quoteList.add("Мы должны признать очевидное: понимают лишь те, кто хочет понять.\nБернар Вербер");
    }

    public String getRandQuote() {
        int randValue = (int) (Math.random() * quoteList.size());

        return quoteList.get(randValue);
    }
}
